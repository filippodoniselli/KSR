# Kitchen Store Manager

Questo esercizio richiede la creazione di un'applicazione (APP.WPF) con interfaccia grafica e database di supporto per la gestione di un magazzino contenente articoli di cucina.

Sarà richiesto l'uso specifico di alcune librerie software: 
1.	XAML
2.	SQL Server Lite
3.	MahApps.Metro

Sarà richiesto l'utilizzo di Git per il versioning del codice e di GitKraken Board per il tracking delle attività svolte.

# Risultato atteso

1.	Un applicativo dotato di UI (azzurra), il quale si appoggia ad un DataBase già esistente, in caso contrario lo crea, modella (e popola?) all’avvio

2.	I prodotti sono suddivisi in categorie e sottocategorie idonee, le quali si strutturano all’interno di classi e interfacce

3.	Partendo dalla UI, l’utente può effettuare operazioni CRUD in base al suo livello di accesso

    •	Dipendente semplice: può solo visualizzare il contenuto del magazzino (READ)

    •	Responsabile magazzino: può visualizzare e modificare quantità dei prodotti (READ-UPDATE)
    
    •	Amministratore: può compiere qualsiasi operazione sui prodotti del magazzino (CRUD). Inoltre, conferisce i privilegi di accesso agli utenti autorizzati

4.	Sistema di login e registrazione per gli utenti

# Ottimizzazione

Non è richiesta ottimizzazione in termini di tempo speso e qualità del codice.

# Documentazione

Verrà presentata una documentazione, la quale spiegherà nel dettaglio il funzionamento dell’applicazione.

# Tempi e Qualità Attesi

Tempo: Illimitato, dipendente dagli impegni personali.

Qualità: uso di tutte le librerie di cui sopra. Devono essere rispettate TUTTE le consegne
